# Event Agenda Monitoring App

Light single-page web-app for displaying dynamic event (agenda) points with timestamps. Best for presenting a time based event agenda or custom schedule on a monitor, tablet or mobile device. 

#event planner, #agenda, #to-do list, #day planner, #scheduler, #event monitoring

- Highly customizable and editable directly on the frontend page
- No database requires, changes are instantly saved in browser localStorage and can be exported/imported into another view
- Live updating list view (1 minute refresh rate)


![Screenshot](/screenshots/eam_display.jpg)

## How to use
- click on the hidden `(+)` icon on the center-bottom of the page to add new events to the list
- click on the hidden `gear` icon on the top-right corner of the page to open the settings
- click on time, name of the event or title/subtitle to change the (input-) text directly

## DEMO / LIVE VERSION

https://dascal.gitlab.io/event-agenda-monitor/ 


### Web tool used in this project
- [Node.js](https://nodejs.org/en/) (npm) : 📄 [Doc](https://nodejs.org/docs/latest-v15.x/api/) - JavaScript runtime built engine and package library
- [Vue](https://v3.vuejs.org/) : 📄 [Doc](https://v3.vuejs.org/guide/introduction.html) - Progressive
JavaScript framework for UI webapplications with [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup)
- [Vite](https://vitejs.dev/) : 📄 [Doc](https://vitejs.dev/guide/) - Frontend tool for fast server and development 
- [PrimeVue](https://primevue.org) : 📄 [Doc](https://primevue.org/vite/) - CSS framework & front-end library 
- [Fontawesome Icons](https://fontawesome.com/icons) : 📄 [Doc](https://fontawesome.com/docs/web/use-with/vue/) - Icon Pack

Recommended IDE editor: [VSCode](https://code.visualstudio.com) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) Plugin

## Getting started (Development)
### Requirenments:
 
1. Install one of the Package Managers if missing:
   - `npm` - [Node.js](https://nodejs.org/en/) when not installed (check for >`npm` command in your shell) (*recomended*)
   - `yarn` - [Yarnpkg](https://yarnpkg.com/) when not installed (check for >`yarn` command in your shell)
   - `pnpm` - [pNPm](https://pnpm.io/) when not installed (check for >`pnpm` command in your shell)
2. Install `git` (Git-Bash) if not installed (check for >`git` command in your shell)

### Import the project to your local envirenment

3. Open your shell and navigate to the path, where you want to create the project.
4. Type `git clone git@gitlab.com:Dascal/event-agenda-monitor.git` 
   to download the project from gitlab to your local machine. The folder "event-agenda-monitor" will be created. 
5. Type your GitLab/GitHub username and password in the promt when asked
6. Jump to the created folder with `cd event-agenda-monitor` 
7. Run `npm install` (npm package manager) to install all needed packages from package.json (vite/vue)

### Running Project and Development Server

By running `nmp run dev` or `npx vite` you will start the development server on your localhost

- `npx vite` - starts dev server under http://localhost:3000
- `npx vite build` - build final project for production in "dist" folder
- `npx vite build --report` - build for production and view the bundle analyzer report
- `npx vite preview` - locally preview the production build

## TODO
- add mobile device functionality (replace: css hover effect, fixing responsive design)