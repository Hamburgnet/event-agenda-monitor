import { createApp } from "vue"
import App from './App.vue'

// Add PrimeVue + components (https://primevue.org)
import 'primevue/resources/themes/lara-light-green/theme.css';
import PrimeVue from 'primevue/config'
import Button from 'primevue/button' 
import InputText from 'primevue/inputtext'
import Sidebar from 'primevue/sidebar' 
import ColorPicker from 'primevue/colorpicker'
import Message from 'primevue/message' 
import InputSwitch from 'primevue/inputswitch'
import Slider from 'primevue/slider'
import Dropdown from 'primevue/dropdown'
import Dialog from 'primevue/dialog'
import Toast from 'primevue/toast'
import ToastService from 'primevue/toastservice'

// Add FontAwesome Icons
import { library } from '@fortawesome/fontawesome-svg-core'         // core
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'      // component
// single specific icons import
import { faCirclePlus, faPlus, faMinus, faCog, faPaintBrush, faSort,faCaretRight, faClipboard } from '@fortawesome/free-solid-svg-icons'  
import { faPenToSquare, faTrashCan } from '@fortawesome/free-regular-svg-icons'  
library.add(faPenToSquare, faCirclePlus, faTrashCan, faPlus, faMinus, faCog, faPaintBrush, faSort, faCaretRight, faClipboard)

// Add own custom CSS
import './css/main.css'

const app = createApp(App)

// Register PrimeVue
app.use(PrimeVue, {ripple: true})
app.use(ToastService)
app.component('Button', Button)
app.component('InputText', InputText)
app.component('Sidebar', Sidebar)
app.component('ColorPicker', ColorPicker)
app.component('Message', Message)
app.component('InputSwitch', InputSwitch)
app.component('Slider', Slider)
app.component('Dropdown', Dropdown)
app.component('Dialog', Dialog)
app.component('Toast', Toast)

// Register FontAwesome
app.component('font-awesome-icon', FontAwesomeIcon)

//mount full App to #app element
app.mount('#app')