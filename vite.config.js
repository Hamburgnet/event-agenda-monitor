import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // gitlab setting: update the BASE_URL value to match the project name (CI_PROJECT_NAME = gitlab project name)
  base: '/event-agenda-monitor/',
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/',
  build: {
    emptyOutDir: true,  // delete content of the build folder before new build
    //outDir: 'F:/Xampp/htdocs/'  // for local tests only. remove this line to use /dist as default output path
  }
})